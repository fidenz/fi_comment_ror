require_dependency 'fzcomment/application_controller'

module Fzcomment
  class ReviewsController < ApplicationController
    def create
      review = Review.new(filter_params)
      review.stars = params[:score]
      review.user_id = current_user.id if user_signed_in?
      username = if user_signed_in?
                   current_user.first_name.to_s
                 else
                   params[:review][:name].to_s
                 end
      review.name = username
      review.save!
      flash[:success] = "#{username}, your review has been added successfully! Thank you."
      redirect_to http_referer
    end

    private

    def filter_params
      params.require(:review).permit(:review, :reviewable_type, :reviewable_id, :title, :name, :email)
    end

    def http_referer
      params['http_referer'].blank? ? :back : params['http_referer']
    end
  end
end
