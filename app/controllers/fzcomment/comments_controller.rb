require_dependency 'fzcomment/application_controller'

module Fzcomment
  class CommentsController < ApplicationController
    before_action :set_comment, only: [:show, :edit, :update, :destroy]

    # GET /comments
    def index
      @comments = Comment.all
    end

    # GET /comments/1
    def show
    end

    # GET /comments/new
    def new
      @comment = Comment.new
    end

    # GET /comments/1/edit
    def edit
    end

    # POST /comments
    def create
      comment = Comment.new(comment_params)
      unless params[:parent_id].nil?
        comment.resource_id = params[:parent_id]
        comment.resource_type = 'Fzcomment::Comment'
      end
      comment.namespace = 'admin'
      unless verify_recaptcha(model: comment)
        flash[:error] = 'It appears the CAPTCHA you entered is invalid, please try again.'
        redirect_to :back and return
      end
      comment.author_id = current_user.id
      comment.author_type = 'User'
      comment.save!
      flash[:success] = 'Thank you for submitting your comment'
      redirect_to :back and return
    end

    # PATCH/PUT /comments/1
    def update
      if @comment.update(comment_params)
        flash[:success] = 'Comment updated.'
        redirect_to @comment and return
      else
        render :edit
      end
    end

    # DELETE /comments/1
    def destroy
      @comment.destroy
      flash[:success] = 'Comment deleted.'
      redirect_to comments_url
    end

    private

    def set_comment
      @comment = Comment.find(params[:id])
    end

    def comment_params
      params.require(:comment).permit(:body, :resource_type, :resource_id)
    end
  end
end
