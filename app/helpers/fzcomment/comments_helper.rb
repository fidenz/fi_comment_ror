module Fzcomment
  module CommentsHelper
    def comments_list(resource, options: {})
      options = options.reverse_update(
        'can_reply' => true
      )
      resource_type, resource_id = Fzcomment.resource(resource)
      comments = resource.comments.active.order(id: :desc).page(params[:page]).per(3)
      @comment = Fzcomment::Comment.new
      render  template: 'fzcomment/comments/index', locals: {
        comments: comments,
        resource_type: resource_type,
        resource_id: resource_id,
        options: options
      }
    end
  end
end
