module Fzcomment
  module ReviewsHelper
    def review_list(resource, options: {})
      options = options.reverse_update(
        'http_referer' => '',
        'template' => nil
      )
      template_url = options['template'].nil? ? 'fzcomment/reviews/index' : options['template']
      reviewable_type, reviewable_id = Fzcomment.resource(resource)
      reviews = resource.reviews.active.order(id: :desc).page(params[:page]).per(3)
      render  template: template_url, locals: {
        reviews: reviews,
        reviewable_type: reviewable_type,
        reviewable_id: reviewable_id,
        options: options
      }
    end

    def stars_count(resource)
      resource.reviews.active.average(:stars).to_i
    end
  end
end
