module Fzcomment
  class Review < ActiveRecord::Base
    acts_as_paranoid

    belongs_to :reviewable, polymorphic: true

    validates :stars, :review, presence: true
    validates :name, presence: true, if: 'user_id.nil?'

    scope :active, -> { where(active: true) }
    before_create :set_defaults

    def author
      user_class = 'User'.constantize
      user_class.with_deleted.find(user_id)
    end

    def author_name
      return name if user_id.nil?
      author.first_name + ' ' + author.last_name
    end

    private

    def set_defaults
      self.active ||= true
      true
    end
  end
end
