module Fzcomment
  class Comment < ActiveRecord::Base
    self.table_name = 'active_admin_comments'
    include Wisper.model
    acts_as_paranoid

    belongs_to :resource, polymorphic: true
    belongs_to :author, -> { with_deleted }, polymorphic: true
    has_many :replies, as: :resource, class_name: 'Fzcomment::Comment', dependent: :destroy

    before_create :set_defaults

    scope :active, -> { where(active: true) }

    def title
      resource.title
    end

    private

    def set_defaults
      self.active ||= true
      true
    end
  end
end
