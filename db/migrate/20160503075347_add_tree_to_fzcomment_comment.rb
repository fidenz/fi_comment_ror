class AddTreeToFzcommentComment < ActiveRecord::Migration
  def change
    add_column :active_admin_comments, :active, :boolean
    change_column :active_admin_comments, :resource_id, 'integer USING CAST(resource_id AS integer)'
  end
end
