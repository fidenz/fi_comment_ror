class AddSoftDeleteCol < ActiveRecord::Migration
  def change
    add_column :active_admin_comments, :deleted_at, :datetime
    add_column :fzcomment_reviews, :deleted_at, :datetime
  end
end
