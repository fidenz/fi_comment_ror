class CreateFzcommentReviews < ActiveRecord::Migration
  def change
    create_table :fzcomment_reviews do |t|
      t.string :name, null: true
      t.references :user, null: true
      t.string :email, null: true
      t.string :title, null: true
      t.text :review, null: false
      t.integer :stars, null: false
      t.references :reviewable, polymorphic: true, index: true
      t.boolean :active
      t.timestamps
    end
  end
end
