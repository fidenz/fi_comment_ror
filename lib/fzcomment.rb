require 'fzcomment/engine'

module Fzcomment
  def self.resource(resource)
    resource_type = resource.class.name
    resource_id = resource.id
    [resource_type, resource_id]
  end
end
