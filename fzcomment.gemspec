$LOAD_PATH.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'fzcomment/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'fzcomment'
  s.version     = Fzcomment::VERSION
  s.authors     = ['oz']
  s.email       = ['oz@example.com']
  s.homepage    = 'https://bitbucket.org/example/fi_comment_ror'
  s.summary     = 'Summary of Fzcomment.'
  s.description = 'Description of Fzcomment.'
  s.license     = 'Nonstandard'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.rdoc']
  s.test_files = Dir['test/**/*']

  s.add_runtime_dependency 'rails' #, '~> 4.2', '>= 4.2.5'
  s.add_runtime_dependency 'recaptcha', '~> 3.0' # , require: 'recaptcha/rails'
  s.add_runtime_dependency 'jquery-raty-rails' # , github: 'bmc/jquery-raty-rails'
  s.add_runtime_dependency 'wisper'
  s.add_runtime_dependency 'wisper-activerecord'
  s.add_runtime_dependency 'paranoia'
end
